- S pomocí PCA můžeme implementovat jednoduché rozpoznávání obličejů, na které jsme PCA předtím “trénovali”
- Pro všech 100 vstupních obrázků ze souboru v gitu provedeme dohromady PCA a ponecháme si prvních 50 komponent
- Potřebné proměnné exportujeme (save workspace) 
- V druhé části scriptu načteme uložené hodnoty (load “jmeno workspace”)
- Na základě testovacích obrázků ze složky faces/test bude umět script odhadnout, o obličej jaké osoby se jedná
- Použijte L1 distance (výsledný obraz má minimální vzdálenostní hodnotu)
- Výsledky vhodně prezentujte = test vs. nejbližší obraz
- Pracujeme s šedotónovými obrazy (nutný převod)
- Pokud by se vyskytl problém s pamětí, lze zmenšit velikost (např. na 100*90)
- Termín odevzdání je do příštího cvičení, tedy 23.11.2020 do 14:00 v gitu ve složce v odpovídající struktuře
- v případě dotazů mě kontaktujte na MS Teams